import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
// import 'rxjs/add/operator/map';
 import 'rxjs/add/operator/catch';
import { catchError, map, tap } from 'rxjs/operators';
import { Book } from '../model/book';
// import { HEROES } from './mock-heroes';

@Injectable()
export class BookService {

  private bookUrl = 'https://59ca6b887e21980011956318.mockapi.io/books';
   // 'http://localhost/book-management/public/api/books';  // URL to web api

  private headers = new HttpHeaders({ 'Content-Type': 'application/json' })
  // const headers = new Headers();


  constructor(private http: HttpClient) { }

  getBooks(page: number, itemPerpage: number): Observable<Book[]> {

    const url = `${this.bookUrl}?p=${page}&l=${itemPerpage}`;
   // alert(url);
    return this.http.get<Book[]>(url)
    .pipe(
      catchError( this.handleError )
    );


  }



  getBook(id: number): Observable<Book> {
    const url = `${this.bookUrl}/${id}`;
    console.log(url);
    return this.http.get<Book>(url).pipe(
      // tap(h => {
      //   alert(h.toString);
      //   const outcome = h ? `fetched` : `did not find`;
      //   console.log(`${outcome} book id=${id}`);

      // }),

          catchError( this.handleError )
        );
  }


  update( book: Book):  Observable<Book> {

    const url = `${this.bookUrl}/${book.id}`;
    // alert(url);
    return this.http
      .put(url, book, {headers: this.headers}).pipe(

        catchError(this.handleError)
      );

  }


  create(book: Book): Observable<Book> {
    return this.http
      .post(this.bookUrl , book, {headers: this.headers})
      .pipe(

        catchError(this.handleError)
      );
  }

  delete(book: Book): Observable<Book> {
    const url = `${this.bookUrl}/${book.id}`;
    console.log(url);
    return this.http.delete(url, {headers: this.headers})
    .pipe(

      catchError(this.handleError)
    );
  }

  private handleError(error: any): Observable<any> {
    //console.error('An error occurred', error); // for demo purposes only
    return Observable.throw(error.json().error  ||  {message: 'Server Error'});
  }

}


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
