import { Component, OnInit , OnDestroy} from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import {Book} from '../model/book';
import {BookService} from '../service/book.service';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

@Component({
  selector: 'ng-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit, OnDestroy {

  book= new  Book();
  public _id: number;
  public subscription: Subscription;
  constructor(
    private bookService: BookService,
    private route: ActivatedRoute,
    private location: Location) {}

  ngOnInit(): void {
    //   this.subscription = this.route.params.subscribe(params => {
    //     this._id = params['id'];
    // });
    this.route.params.subscribe(params1 => {
      console.log(params1);
      console.log(+params1['id']);
      const id = params1['id'];
      if ( id ) {
        this.route.paramMap
        .switchMap((params: ParamMap) => this.bookService.getBook(+params.get('id')))
        .subscribe(book => this.book = book);
       // console.log(this.book.content);
      }

   });


    // .catch(error => {
    //   // TODO: add real error handling
    //   console.log(error);
    //   return Observable.of<Book>();
    // });
  }

  goBack(): void {
    this.location.back();
  }

  saveBook(): void {
    this.bookService.create(this.book).toPromise()
      .then(() => this.goBack());
  }

  updateBook(): void {
    this.bookService.update(this.book).subscribe(response => {
      if (response) {
          alert('Save success');
          this.goBack();
      }
    });
  }


  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

  }
}
