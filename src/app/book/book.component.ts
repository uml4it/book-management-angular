import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Book} from '../model/book';
import {BookService} from '../service/book.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
@Component({
  selector: 'ng-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit{


  itemsPerPage= 10;
  totalItems= 100;
  page= 1;
   previousPage= 1;
  books: Book[];

  constructor(
    private bookService: BookService) {}

  ngOnInit(): void {
        this.loadData();
       }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.loadData();
    }
  }


    loadData() {
      //const page =  this.page - 1;
      const size = this.itemsPerPage;
      console.log('helllllllo' + this.page + ' -- ' + size);
      this.bookService.getBooks(this.page, size).subscribe(books => this.books = books);

    }

  deleteBook(book: Book): void {
     if ( confirm('Are you sure to delete ' + book.name )) {
      console.log('Implement delete functionality here');

       this.bookService.delete(book).subscribe();
     }

   }

}
